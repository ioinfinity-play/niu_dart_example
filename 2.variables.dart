main() {
  // String:
  String name = 'Sandy';
  //print(name);

  // Integer:
  int age = 3;
  //print(age);
  //print("Age: " + age.toString());

  // Double:
  double height = 104.5;
  height = 105.2;
  //print(heigh);
  //print("Height: " + heigh.toString());

  // Final:
  final double weight = 20.3;
  //weight = 10;
  //print(weight);
  //print("Weight: " + weight.toString());

  // Boolean:
  bool at_school = true;
  //print(at_school);
  //print("At school: " + at_school.toString());

  //List:
  List play_time_hours = [1, 2, 3, 4];
  //print(play_time_hours);

  play_time_hours[0] = 6;
  //print("Modify the first element 1 -> 6.");
  //print(play_time_hours);
  //print(play_time_hours.last);
  //print(play_time_hours.first);
  //print(play_time_hours.isEmpty);

  play_time_hours.removeAt(0);
  //print("Remove the first element.");
  //print(play_time_hours);

  // Map:
  Map profile = {
    'name': 'Sandy',
    'age': 3,
    'height': 104.5,
    'weight': 20.5,
  };
  //print(profile);
}
