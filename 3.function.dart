hello_name_1(String name) {
  return "Hello, " + name + "!";
}

hello_name_2(String name) => "Hello, " + name + "!";
hello_name_3(String name) => print("Hello, " + name + "!");

main() {
  String sandy_name = "Sandy";
  print(hello_name_1(sandy_name));
  print(hello_name_2(sandy_name));
  hello_name_3(sandy_name);
}
