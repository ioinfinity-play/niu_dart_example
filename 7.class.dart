class People {
  String name = "";
  int age = 0;
  void setName(String newName) {
    this.name = newName;
  }

  String getName() {
    return this.name;
  }

  void setAge(int newAge) {
    this.age = newAge;
  }

  int getAge() {
    return this.age;
  }
}

main() {
  People sandy = new People();
  sandy.setAge(4);
  sandy.setName('Sandy');
  print(sandy);
  print(sandy.getName());
}
