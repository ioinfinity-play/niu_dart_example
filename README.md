# NIU - Dart Examples

## Dart commands

* Print this usage information.

> dart -h

* Print the Dart SDK version.

> dart -v


## Download this repository (Git Bash terminal on Windows OS.)

> cd /c/

> mkdir -p /c/flutter_projects

> cd /c/flutter_projects

> git clone https://gitlab.com/ioinfinity-play/niu_dart_example.git

> ls /c/flutter_projects/niu_dart_example

> cd /c/flutter_projects/niu_dart_example

> Import C:\flutter_projects\niu_dart_example folder to your Visual Studio code editor.

## Run Dart example file

> dart {your dart file name}.dart


> dart 1.hello_world.dart

> dart 2.variables.dart

> dart 3.function.dart

> dart 4.operation.dart

> dart 5.decision_making.dart

> dart 6.loop.dart

> dart 7.class.dart


## Reference

* https://www.tutorialspoint.com/dart_programming/index.htm
